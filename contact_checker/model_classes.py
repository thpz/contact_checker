#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime

from peewee import *
from playhouse.reflection import Introspector

db = SqliteDatabase(None)
introspector = Introspector.from_database(db)


class Statistics:

    def __init__(self):
        self.contacts = 0
        self.in_linkedin_group = 0
        self.not_in_linkedin_group = 0
        self.linkedin_unchecked = 0
        self.without_linkedin_account = 0
        self.with_linkedin_account = 0
        self.invited_to_linkedin_group = 0
        self.not_invited_to_linkedin_group = 0


class BaseModel(Model):

    class Meta:
        database = db


class Masterversion(BaseModel):
    migrationLevel = IntegerField(default=0)


class Run(BaseModel):
    fileName = CharField(null=False, default='')
    insertDate = DateTimeField(default=datetime.datetime.now())


class User(BaseModel):
    email = CharField(unique=True)
    firstName = CharField(default='')
    lastName = CharField(default='')
    run = ForeignKeyField(Run)
    insertDate = DateTimeField(default=datetime.datetime.now())

    class Meta:
        order_by = ('email',)


class LinkedIn(BaseModel):
    user = ForeignKeyField(User, related_name='linkedin', unique=True)
    isAlreadyConnected = BooleanField(default=False)                # Bereits bei LinkedIn befreundet
    hasLinkedIn = BooleanField(default=False)                       # Hat einen LinkedIn-Account
    isInvited = BooleanField(default=False)                         # Schon eingeladen?
    insertDate = DateTimeField(default=datetime.datetime.now())
    updateDate = DateTimeField(default=datetime.datetime.now())


class XingUser(BaseModel):
    user_id = CharField(unique=True, null=False)
    first_name = CharField(default='')
    last_name = CharField(default='')
    insertDate = DateTimeField(default=datetime.datetime.now())


class XingGroup(BaseModel):
    group_id = CharField(unique=True, null=False)
    name = CharField(default='')
    insertDate = DateTimeField(default=datetime.datetime.now())


class XingInvitation(BaseModel):
    user = ForeignKeyField(XingUser, related_name='isInvitedTo')
    group = ForeignKeyField(XingGroup, related_name='Invitations')
    invitationDate = DateTimeField(default=datetime.datetime.now())


class XingWorkingTable(BaseModel):
    user = ForeignKeyField(XingUser, related_name='isListedFor')
    group = ForeignKeyField(XingGroup, related_name='ListedUsers')
    is_invited = BooleanField(default=False, null=False)
