#!/usr/bin/env python
# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup


class XingScraper(object):

    @staticmethod
    def get_all_groups(code):
        soup = BeautifulSoup(code, 'html5lib')
        groups = [(g['value'], g.text) for g in soup.find_all(name='option') if g['value']]
        return groups

    @staticmethod
    def get_users(code):
        users = []
        soup = BeautifulSoup(code, 'html5lib')
        for member in soup.find_all(name='div', class_='member search-result'):
            xing_id = None
            vorname = ''
            nachname = None

            for link in member.find_all(name='a'):
                if link.get('data-reco-action') == 'add':
                    xing_id = link['data-add-reco-sid'].split('.')[0]

            if xing_id:
                div = member.find(name='div', class_='bd')
                link = div.find(name='a', class_='name-page-link')
                nachname = link.text

            if xing_id and nachname:
                user = (int(xing_id), vorname, nachname)
                users.append(user)
            else:
                raise RuntimeError()

        return users

    @staticmethod
    def get_users_next_page_link(code):
        soup = BeautifulSoup(code, 'html5lib')
        for link in soup.find_all(name='a', rel='next'):
            if link.text == 'Weiter':
                return link
        return None

    @staticmethod
    def get_group_members(code):
        users = []
        soup = BeautifulSoup(code, 'html5lib')
        member_list = soup.find_all(name='div', id='members-list-container')[0]
        for member in member_list.find_all(name='li'):
            xing_id = None
            vorname = ''
            nachname = None

            for link in member.find_all(name='a'):
                href = link.get('href')
                if 'op=add' in href:
                    xing_id = href.split('b_sid=')[1].split('.')[0]

                if link.attrs.get('class') and link.attrs.get('class')[0] == 'user-name':
                    nachname = link.text

            if xing_id and nachname:
                user = (int(xing_id), vorname, nachname)
                users.append(user)

        return users

    @staticmethod
    def get_group_members_next_page_link(code):
        soup = BeautifulSoup(code, 'html5lib')
        for link in soup.find_all(name='a'):
            if link.text == 'Weiter':
                return link
        return None

xing_scraper = XingScraper()
