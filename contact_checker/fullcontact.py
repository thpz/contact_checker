#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import time

from datetime import datetime, timedelta


# https://www.fullcontact.com/developer/docs/rate-limits/
class FullContactAdaptiveClient:

    REQUEST_LATENCY = 0.2

    def __init__(self, api_key):
        self.api_key = api_key
        self.next_req_time = datetime.fromtimestamp(0)

    def call_fullcontact(self, email):
        self._wait_for_rate_limit()
        r = requests.get('http://api.fullcontact.com/v2/person.json',
                         params={'email': email, 'apiKey': self.api_key})
        self._update_rate_limit(r.headers)
        return r.json()

    def _wait_for_rate_limit(self):
        now = datetime.now()
        if self.next_req_time > now:
            t = self.next_req_time - now
            time.sleep(t.total_seconds())

    def _update_rate_limit(self, hdr):
        limit_remaining = hdr.get('X-Rate-Limit-Remaining')
        limit_reset = hdr.get('X-Rate-Limit-Reset')

        if not limit_remaining or not limit_reset:
            return

        remaining = float(limit_remaining)
        reset = float(limit_reset)
        spacing = reset / (1.0 + remaining)
        delay = spacing - self.REQUEST_LATENCY
        self.next_req_time = datetime.now() + timedelta(seconds=delay)


class FullContactLinkedInCheck:

    def __init__(self, api_key, users):
        self.api_key = api_key
        self.users = users
        self.has_linkedin = list()
        self.has_not_linkedin = list()
        self.repeat_check = False
        self.invalid_key = False

    def run(self, updater=None):
        client = FullContactAdaptiveClient(self.api_key)
        n = 0
        for user in self.users:
            n += 1
            if updater:
                updater.run(msg='Checke User {} von {}'.format(n, self.users.__len__()))

            r = client.call_fullcontact(user['email'])
            rc = r.get('status')

            # https://www.fullcontact.com/developer/docs/person/#person-api-diagrams
            if rc == 404:
                # Kein Treffer bei FullContact
                self.has_not_linkedin.append(user)
            elif rc == 200:
                # Treffer bei FullContact
                try:
                    profiles = r['socialProfiles']
                    hit = False
                    for p in profiles:
                        if p.get('type') == 'linkedin':
                            # LinkedIn-Profil gefunden
                            hit = True
                    if hit:
                        self.has_linkedin.append(user)
                    else:
                        self.has_not_linkedin.append(user)
                except KeyError:
                    continue
            elif rc == 202:
                # FullContact sucht noch
                self.repeat_check = True
            elif rc == 403:
                self.invalid_key = True
                break
            elif rc == 422:
                # Invalid or missing query parameter
                # Tritt auf, falls E-Mail-Adresse nicht dem o.k. (z.B. Leerzeichen, o.ä.)
                self.has_not_linkedin.append(user)
            elif 500 <= rc < 600:
                raise RuntimeError(u'Unbekannte Fehler bei Full Contact!\n'
                                   u'Bitte später noch einmal probieren.')
            else:
                # print u'email: {}'.format(user['email'])
                raise RuntimeError(u'Unbekannter Fehler!\n'
                                   u'Bitte Entwickler kontaktieren.\n'
                                   u'(RC={})'.format(rc))
