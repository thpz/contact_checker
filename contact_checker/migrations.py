#!/usr/bin/env python
# -*- coding: utf-8 -*-

from contact_checker.model_classes import db, introspector, Run, Masterversion, User, LinkedIn
from contact_checker.model_classes import XingUser, XingGroup, XingInvitation, XingWorkingTable
from contact_checker.model import mainmodel


class Migration001:

    def __init__(self):
        self.chunk_size = 500
        self.contacts_model = None

    def insert_many(self, model, entries):
        with db.atomic():
            for idx in range(0, len(entries), self.chunk_size):
                model.insert_many(entries[idx:idx + self.chunk_size]).execute()

    def contacts2user(self):
        contacts = list()
        query = self.contacts_model.select()
        for contact in query.execute():
            contacts.append({'email': contact.email, 'firstName': contact.vorname, 'lastName': contact.nachname})
        mainmodel.create_new_users(contacts)

    def contacts2user_already_connected(self):
        contacts = list()
        query = self.contacts_model.select().where(self.contacts_model.linkedin_conn == 1)
        for contact in query.execute():
            user = User.get(User.email == contact.email)
            contacts.append({'user': user, 'isAlreadyConnected': True,
                             'hasLinkedIn': True, 'insertDate': contact.fullcontact_linkedin_ts})
        self.insert_many(LinkedIn, contacts)

    def contacts2user_with_linkedin(self):
        contacts = list()
        query = self.contacts_model.select().where(self.contacts_model.fullcontact_linkedin == 3)
        for contact in query.execute():
            user = User.get(User.email == contact.email)
            contacts.append({'user':    user, 'hasLinkedIn': True, 'insertDate': contact.fullcontact_linkedin_ts})
        self.insert_many(LinkedIn, contacts)

    def contacts2user_without_linkedin(self):
        contacts = list()
        query = self.contacts_model.select().where(self.contacts_model.fullcontact_linkedin == 2)
        for contact in query.execute():
            user = User.get(User.email == contact.email)
            contacts.append({'user': user, 'hasLinkedIn': False, 'insertDate': contact.fullcontact_linkedin_ts})
        self.insert_many(LinkedIn, contacts)

    def run(self):
        if Masterversion.table_exists():
            return True

        Masterversion.create_table(fail_silently=True)
        Run.create_table(fail_silently=True)
        User.create_table(fail_silently=True)
        LinkedIn.create_table(fail_silently=True)

        Masterversion.create()

        models = introspector.generate_models()
        try:
            self.contacts_model = models['CONTACTS']
        except KeyError:
            return

        mainmodel.run = Run.create(fileName='---')
        self.contacts2user()
        self.contacts2user_already_connected()
        self.contacts2user_with_linkedin()
        self.contacts2user_without_linkedin()
        self.contacts_model.drop_table(fail_silently=True)


class Migration002(object):

    @staticmethod
    def run():
        if Masterversion.get().migrationLevel >= 1:
            return

        Masterversion.update(migrationLevel=1).execute()

        XingUser.create_table(fail_silently=True)
        XingGroup.create_table(fail_silently=True)
        XingInvitation.create_table(fail_silently=True)
        XingWorkingTable.create_table(fail_silently=True)


class Migrations:
    def __init__(self):
        self.migration001 = Migration001()
        self.migration002 = Migration002()

    def run(self):
        self.migration001.run()
        self.migration002.run()


migrations = Migrations()
