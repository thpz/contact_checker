#!/usr/bin/env python
# -*- coding: utf-8 -*-
import cookielib
import logging
import mechanize
import time

from contact_checker.xing_scraper import xing_scraper
from contact_checker.xing_model import xing_mainmodel

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class XingController:

    def __init__(self):
        self.browser = None
        self.username = ''
        self.groups = []
        self.selected_group = None
        self.users_found = 0

        # Eher privat, könnte mal ausgelagert werden
        self.updater = None
        self.step = 0
        self.total_steps = 0

    def display_step(self, message):
        if self.updater:
            self.updater.run(self.step, self.total_steps, message)
            self.step += 1
        logger.info(message)

    def display_final_message(self, message):
        if self.updater:
            self.updater.set_final_message(message)
        logger.info(message)

    def perform_login(self, updater, username, password):

        self.username = ''

        self.updater = updater
        self.step = 0
        self.total_steps = 3

        # Predicate to select the login form by action
        def select_form(form):
            return form.attrs.get('action', None) == 'https://login.xing.com/login'

        try:
            self.display_step(u'Öffnen des Browsers')
            self.browser = mechanize.Browser()
            self.browser.set_cookiejar(cookielib.CookieJar())
            self.browser.set_handle_robots(False)
            self.browser.open('https://www.xing.com/signup?login=1')
        except:
            logger.exception('message')
            raise RuntimeError(u'Browser konnte nicht geöffnet werden')

        time.sleep(5)

        try:
            self.display_step(u'Sende Login-Formular')
            self.browser.select_form(predicate=select_form)
            self.browser.form['login_form[username]'] = username
            self.browser.form['login_form[password]'] = password
            self.browser.submit()
        except:
            logger.exception('message')
            raise RuntimeError(u'Browser konnte nicht geöffnet werden')

        try:
            self.display_step(u'Prüfen ob Login erfolgreich')
            self.browser.select_form(predicate=select_form)
            raise RuntimeError(u'Login fehlgeschlagen!')
        except mechanize.FormNotFoundError:
            pass

        time.sleep(3)
        try:
            self.display_step(u'Lade Gruppen')
            self.browser.open('https://www.xing.com/communities/users/7628952/invitations/new')
            groups = xing_scraper.get_all_groups(self.browser.response().read())
            for g in groups:
                xing_mainmodel.add_group(group_id=int(g[0]), group_name=g[1])
        except:
            logger.exception('message')
            raise RuntimeError(u'Gruppen konnten nicht geladen werden.\n'
                               u'Haben Sie eine Datenbank ausgewählt?')

        self.username = username
        self.groups = groups
        self.display_final_message(u'Einloggen auf XING erfolgreich!')

    def clear_working_table(self):
        xing_mainmodel.delete_working_table(group_id=self.selected_group[0])

    def search_group_members(self, updater, group_id, limit):
        self.updater = updater
        self.step = 0
        self.total_steps = 2

        self.users_found = 0

        try:
            limit = int(limit)
            if limit <= 0:
                raise ValueError
        except ValueError:
            raise RuntimeError(u'Limit muss eine ganze Zahl größer als Null sein!')

        try:
            self.display_step('Lade Such-Formular')

            url = 'https://www.xing.com/communities/groups/{}/member_states' \
                  '?sc_o=b6733_srt_rmf&sorting=newest_first&update_filters=true'.format(group_id.strip())
            self.browser.open(url)
            time.sleep(3)
        except:
            logger.exception('message')
            raise RuntimeError(u'Such-Formular konnte nicht geladen werden')

        try:
            self.display_step('Lade Mitglieder')
            users_found = 0
            while True:
                users = xing_scraper.get_group_members(self.browser.response().read())
                if not users:
                    break

                for user in users:
                    if xing_mainmodel.is_user_invited(user[0], self.selected_group[0]):
                        continue
                    xing_mainmodel.add_user_to_working_list_group(self.selected_group[0],
                                                                  user[0],
                                                                  user[1],
                                                                  user[2])
                    users_found += 1
                    if limit and users_found >= limit:
                        break

                if limit and users_found >= limit:
                    break
                else:
                    try:
                        self.browser.follow_link(text='Weiter')
                    except mechanize.LinkNotFoundError:
                        break
        except:
            raise RuntimeError(u'Mitglieder konnten nicht geladen werden')

        self.display_final_message(u'{} Xing-Mitglieder in Working Table eingetragen'.format(users_found))

    def search_members(self, updater, keywords, wants, haves, limit):

        # Predicate to select the form by action
        def select_form(form):
            return form.attrs.get('action', None) == '/search/members'

        self.updater = updater
        self.step = 0
        self.total_steps = 2

        self.users_found = 0

        try:
            limit = int(limit)
            if limit <= 0:
                raise ValueError
        except ValueError:
            raise RuntimeError(u'Limit muss eine ganze Zahl größer als Null sein!')

        try:
            self.display_step('Lade Such-Formular')
            self.browser.open('https://www.xing.com/search/members?advanced_form=open')
            time.sleep(3)
            self.browser.select_form(predicate=select_form)
            self.browser.form['keywords'] = keywords
            self.browser.form['wants'] = wants
            self.browser.form['haves'] = haves
            self.browser.submit()
        except:
            logger.exception('message')
            raise RuntimeError(u'Such-Formular konnte nicht geladen werden')

        try:
            self.display_step('Lade Mitglieder')
            users_found = 0
            while True:
                users = xing_scraper.get_users(self.browser.response().read())
                if not users:
                    break

                for user in users:
                    if xing_mainmodel.is_user_invited(user[0], self.selected_group[0]):
                        continue
                    xing_mainmodel.add_user_to_working_list_group(self.selected_group[0],
                                                                  user[0],
                                                                  user[1],
                                                                  user[2])
                    users_found += 1
                    if limit and users_found >= limit:
                        break

                if limit and users_found >= limit:
                    break
                else:
                    try:
                        self.browser.follow_link(text='Weiter')
                    except mechanize.LinkNotFoundError:
                        break
        except:
            raise RuntimeError(u'Mitglieder konnten nicht geladen werden')

        self.display_final_message(u'{} Xing-Mitglieder in Working Table eingetragen'.format(users_found))

    def get_statistics(self):
        return xing_mainmodel.get_statistics(self.selected_group[0])

    def get_uninvited(self):
        return xing_mainmodel.get_uninvited(self.selected_group[0])

    def send_invitations(self, updater, subject, message, limit, inv_per_hour):

        # Predicate to select the invitation form by action
        def select_form(form):
            return form.attrs.get('action', None) == '/communities/invitations'

        try:
            limit = int(limit)
            if limit < 0:
                raise ValueError
        except ValueError:
            raise RuntimeError(u'Anzahl der Einladungen muss eine ganze Zahl größer oder gleich Null sein!')

        if limit > 0:
            uninvited = self.get_uninvited()[:limit]
        else:
            uninvited = self.get_uninvited()

        try:
            inv_per_hour = int(inv_per_hour)
            if inv_per_hour <= 0:
                raise ValueError
        except ValueError:
            raise RuntimeError(u'Anzahl der Einladungen pro Stunde muss eine ganze Zahl größer als Null sein!')

        waiting_time = 3600 / inv_per_hour

        self.updater = updater
        self.step = 0
        self.total_steps = uninvited.__len__()

        count = 0
        for user in uninvited:
            count += 1
            self.display_step(u'Lade XING-Mitglied {} ein...'.format(count))

            try:
                url = 'https://www.xing.com/communities/users/{}/invitations/new'.format(user[0])
                # url = 'https://www.xing.com/communities/users/{}/invitations/new'.format(7628952)
                filled_invitation = message.replace('$$NAME$$', user[1].encode('utf-8'))

                self.browser.open(url)
                time.sleep(waiting_time)
                self.browser.select_form(predicate=select_form)
                self.browser.form['group_id'] = [str(self.selected_group[0])]
                self.browser.form['subject'] = subject
                self.browser.form['body'] = filled_invitation
                self.browser.submit()

                xing_mainmodel.invite_user_to_group(self.selected_group[0], user[0])
            except Exception, e:
                logger.exception('message')
                # raise RuntimeError(u'Fehler beim Versenden der Einladungen!\n\n'
                #                    u'{} Einladungen wurden verschickt.'.format(count - 1))
                continue

        self.display_final_message(u'{} Einladung erfolgreich versendet!'.format(count))
