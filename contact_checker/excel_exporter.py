#!/usr/bin/env python
# -*- coding: utf-8 -*-
import codecs
import datetime
import glob
import os


class _ExcelExporter:

    def __init__(self):
        self.header_row = None
        self.row_template = None
        self.limit = None
        self.filename = ''
        self.filename_short = ''

    def run(self, data, export_path):
        file_count = 0
        data_count = 0

        work_file = None
        filename = os.path.join(export_path, self.filename)
        filename_short = os.path.join(export_path, self.filename_short)

        if not self.limit:
            self.limit = data.__len__()

        current_date = datetime.datetime.now().strftime("%Y%m%d")

        if glob.glob(filename_short + '*'):
            raise RuntimeError(u'Es existieren schon Paket-Dateien im ausgewählten Verzeichnis!\n\n'
                               u'Export daher nicht möglich.')

        for element in data:
            if data_count % self.limit == 0:
                file_count += 1
                data_count = 0
                try:
                    if work_file:
                        work_file.close()
                    work_file = codecs.open(filename.format(current_date, file_count), 'w', 'utf-8')
                    work_file.write(self.header_row)
                except IOError, e:
                    raise RuntimeError(u'Exportdatei konnte nicht geöffnet werden!')
            data_count += 1
            work_file.write(self.row_template.format(*element))


class LinkedInExcelExporter(_ExcelExporter):

    def __init__(self, limit=500):
        _ExcelExporter.__init__(self)

        self.header_row = \
            u'First Name,Middle Name,Last Name,Title,Suffix,Initials,Web Page,Gender,Birthday,' \
            u'Anniversary,Location,Language,Internet Free Busy,Notes,E-mail Address,E-mail 2 Address,' \
            u'E-mail 3 Address,Primary Phone,Home Phone,Home Phone 2,Mobile Phone,Pager,Home Fax,' \
            u'Home Address,Home Street,Home Street 2,Home Street 3,Home Address PO Box,Home City,' \
            u'Home State,Home Postal Code,Home Country,Spouse,Children,Manager\'s Name,Assistant\'s Name,' \
            u'Referred By,Company Main Phone,Business Phone,Business Phone 2,Business Fax,' \
            u'Assistant\'s Phone,Company,Job Title,Department,Office Location,Organizational ID Number,' \
            u'Profession,Account,Business Address,Business Street,Business Street 2,Business Street 3,' \
            u'Business Address PO Box,Business City,Business State,Business Postal Code,Business Country,' \
            u'Other Phone,Other Fax,Other Address,Other Street,Other Street 2,Other Street 3,' \
            u'Other Address PO Box,Other City,Other State,Other Postal Code,Other Country,Callback,' \
            u'Car Phone,ISDN,Radio Phone,TTY/TDD Phone,Telex,User 1,User 2,User 3,User 4,Keywords,Mileage,' \
            u'Hobby,Billing Information,Directory Server,Sensitivity,Priority,Private,Categories\n'

        # Platzhalter: Nachname, Vorname, E-Mail
        self.row_template = \
            u'{},,{},,,,,,,,,,,,{},,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,' \
            u',,,,,,,,,,,,,,,,,,,,,,Normal,,Meine Kontakte\n'

        self.filename_short = u'Einladungen_LinkedIn'
        self.filename = self.filename_short + u'_{}_{}.csv'

        self.limit = limit
