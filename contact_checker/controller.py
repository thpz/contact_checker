#!/usr/bin/env python
# -*- coding: utf-8 -*-
from excel_importer import NewContactsImporter, KnowsContactsImporter
from excel_exporter import LinkedInExcelExporter
from model import mainmodel
from model_classes import db, Run
from migrations import migrations
from fullcontact import FullContactLinkedInCheck


class Controller(object):

    @staticmethod
    def get_statistics():
        return mainmodel.get_statistics()

    @staticmethod
    def load_new_contacts_from_excel(updater=None, filename=None):
        if updater:
            updater.run(1, 2, u'Lesen aus Excel-Datei')
        users = NewContactsImporter.import_file(filename)
        if updater:
            updater.run(2, 2, u'Laden in Datenbank')
        mainmodel.create_new_users(users)
        if updater:
            updater.set_final_message(u'Verarbeitung erfolgreich!\n\n'
                                      u'{} Kontakte eingelesen.'.format(users.__len__()))

    @staticmethod
    def load_known_contacts_from_excel(updater=None, filename=None):
        if updater:
            updater.run(1, 2, u'Lesen aus Excel-Datei')
        users = KnowsContactsImporter.import_file(filename)
        if updater:
            updater.run(2, 2, u'Laden in Datenbank')
        mainmodel.create_new_users(users)
        mainmodel.create_new_linkedin(users)
        mainmodel.update_linkedin(users, hasLinkedIn=True, isAlreadyConnected=True)
        if updater:
            updater.set_final_message(u'Verarbeitung erfolgreich!\n\n'
                                      u'{} Kontakte eingelesen.'.format(users.__len__()))

    @staticmethod
    def export_linkedin_invitations(updater=None, export_path=None):
        if updater:
            updater.run(1, 1, u'Erstelle Importdateien für LinkedIn')
        excel_exporter = LinkedInExcelExporter()
        linkedin_list = mainmodel.get_uninvited_linkedin()
        data = [(linkedin.user.lastName, linkedin.user.firstName, linkedin.user.email) for linkedin in linkedin_list]
        excel_exporter.run(data, export_path)
        mainmodel.update_linkedin([dict(email=linkedin.user.email) for linkedin in linkedin_list], isInvited=True)
        if updater:
            updater.set_final_message(u'Verarbeitung erfolgreich!\n\n'
                                      u'{} Kontakte exportiert.'.format(linkedin_list.__len__()))

    @staticmethod
    def check_linkedin_via_fullcontact(updater=None, api_key=None, limit=0):
        try:
            limit = int(limit)
            if limit <= 0:
                raise ValueError
        except ValueError:
            raise RuntimeError(u'Limit muss eine Zahl größer 0 sein!')

        if updater:
            updater.run(1, 3, u'Selektiere User aus Datenbank')
        users = mainmodel.get_users_with_unchecked_linkedin(limit)

        if updater:
            updater.run(2, 3, u'Abgleich via Full Contact')
        checker = FullContactLinkedInCheck(api_key, users)
        checker.run(updater)

        if updater:
            updater.run(3, 3, u'Schreibe LinkedIn-User in Datenbank')
        mainmodel.create_new_linkedin(checker.has_not_linkedin)
        mainmodel.create_new_linkedin(checker.has_linkedin)
        mainmodel.update_linkedin(checker.has_linkedin, hasLinkedIn=True)

        if updater:
            checked = checker.has_linkedin.__len__() + checker.has_not_linkedin.__len__()
            need_recheck = users.__len__() - checked

            if checker.invalid_key:
                message_begin = u'Full Contact Key ungültig oder ausgeschöpft!\n\n'
            else:
                message_begin = u'Verarbeitung erfolgreich!\n\n'

            if checker.repeat_check:
                updater.set_final_message(message_begin +
                                          u'{} Kontakte wurden geprüft.\n'
                                          u'{} Kontakte werden noch von Full Contact gesucht.\n\n'
                                          u'Bitte prüfen Sie diese noch einmal nach 2 Minuten.' \
                                          .format(checked, need_recheck))
            else:
                updater.set_final_message(message_begin +
                                          u'{} Kontakte wurden geprüft.'.format(checked))

    @staticmethod
    def connect2database(filename):
        db.init(filename)
        migrations.run()
        mainmodel.run = Run.create(fileName='Dummy')
