#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from peewee import IntegrityError

from model_classes import db
from model_classes import User, LinkedIn, Run, Statistics


class Model:

    def __init__(self):
        self.chunk_size = 100
        self.run = None

        logger = logging.getLogger('peewee')
        logger.setLevel(logging.INFO)

    def insert_many(self, model, entries):
        try:
            with db.atomic():
                for idx in range(0, len(entries), self.chunk_size):
                    model.insert_many(entries[idx:idx + self.chunk_size]).execute()
        except IntegrityError, e:
            raise RuntimeError('Fehler bei insert_many()')

    def create_new_users(self, userlist):
        new_list = list()
        for user in userlist:
            query = User.select().where(User.email == user['email'])
            if not query.exists():
                user['run'] = self.run
                new_list.append(user)
        self.insert_many(User, new_list)

    def create_new_linkedin(self, userlist):
        new_linkedin = list()
        for user in userlist:
            query = User.select().where(User.email == user['email'])
            user_instance = query.first()
            query = LinkedIn.select().where(LinkedIn.user == user_instance)
            if not query.exists():
                new_linkedin.append(dict(user=user_instance))
        self.insert_many(LinkedIn, new_linkedin)

    @staticmethod
    def update_linkedin(userlist, **kwargs):
        email_list = [user['email'] for user in userlist]
        subquery = User.select().where(User.email << email_list)
        query = LinkedIn.update(**kwargs).where(LinkedIn.user << subquery)
        query.execute()

    @staticmethod
    def get_uninvited_linkedin():
        result = LinkedIn.select(LinkedIn.user).where(LinkedIn.hasLinkedIn == True,
                                                      LinkedIn.isAlreadyConnected == False,
                                                      LinkedIn.isInvited == False)
        return result

    @staticmethod
    def get_statistics():
        stat = Statistics()

        stat.contacts = User.select().count()
        stat.in_linkedin_group = LinkedIn.select().where(LinkedIn.isAlreadyConnected == True).count()
        stat.not_in_linkedin_group = stat.contacts - stat.in_linkedin_group
        stat.with_linkedin_account = LinkedIn.select().where(LinkedIn.hasLinkedIn == True,
                                                             LinkedIn.isAlreadyConnected == False).count()
        stat.without_linkedin_account = LinkedIn.select().where(LinkedIn.hasLinkedIn == False,
                                                                LinkedIn.isAlreadyConnected == False).count()
        stat.linkedin_unchecked =\
            stat.not_in_linkedin_group - stat.with_linkedin_account - stat.without_linkedin_account
        stat.invited_to_linkedin_group = LinkedIn.select().where(LinkedIn.hasLinkedIn == True,
                                                                 LinkedIn.isAlreadyConnected == False,
                                                                 LinkedIn.isInvited == True).count()
        stat.not_invited_to_linkedin_group = LinkedIn.select().where(LinkedIn.hasLinkedIn == True,
                                                                 LinkedIn.isAlreadyConnected == False,
                                                                 LinkedIn.isInvited == False).count()
        return stat

    @staticmethod
    def get_users_with_unchecked_linkedin(limit):
        result = list()
        for user in User.select().order_by(User.insertDate.desc()):
            if user.linkedin.first() is None:
                result.append(dict(email=user.email))
                if result.__len__() >= limit:
                    break
        return result

mainmodel = Model()
