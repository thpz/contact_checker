#!/usr/bin/env python
# -*- coding: utf-8 -*-
import openpyxl


class ExcelImporter(object):

    @staticmethod
    def get_map():
        raise NotImplementedError

    @staticmethod
    def get_key():
        raise NotImplementedError

    @classmethod
    def get_required_headers(cls):
        return cls.get_map().keys()

    @classmethod
    def transform(cls, original_list):
        name_map = cls.get_map()
        key = name_map[cls.get_key()]

        seen = set()
        transformed_list = list()
        for element in original_list:
            element = {name_map[key]: val for key, val in element.items()}
            element[key] = element[key].lower()
            if not element[key] in seen:
                seen.add(element[key])
                transformed_list.append(element)
        return transformed_list

    @classmethod
    def parse_excel_file(cls, filename, updater=None):
        key = cls.get_key()
        required_headers = cls.get_required_headers()

        try:
            wb = openpyxl.load_workbook(filename, read_only=True)
        except Exception:
            raise RuntimeError(u'Exceldatei nicht lesbar.')

        if wb.get_sheet_names().__len__() != 1:
            raise RuntimeError(u'Exceldatei darf nur ein Blatt enthalten.')

        worksheet = wb.active

        # Header suchen
        headers = dict()
        for row in worksheet.iter_rows():
            for index, cell in enumerate(row, start=0):
                if cell.value.lower() in required_headers:
                    headers[cell.value.lower()] = index
            break

        # Alle gefunden?
        if not all(h in headers for h in required_headers):
            raise RuntimeError(u'Folgende Spalten werden benötigt:\n' + '\n'.join(required_headers))

        # Alle Zeile einlesen
        result = list()
        for row in worksheet.iter_rows(min_row=2):
            if not row[headers[key]].value:
                continue
            item = dict()
            for k, v in headers.iteritems():
                item[k] = row[v].value if row[v].value is not None else ''
            result.append(item)

        return result

    @classmethod
    def import_file(cls, filename):
        list_from_file = cls.parse_excel_file(filename)
        return cls.transform(list_from_file)


class NewContactsImporter(ExcelImporter):
    @staticmethod
    def get_map():
        return {'e-mail': 'email', 'vorname': 'firstName', 'nachname': 'lastName'}

    @staticmethod
    def get_key():
        return 'e-mail'


class KnowsContactsImporter(ExcelImporter):
    @staticmethod
    def get_map():
        return {'e-mail address': 'email', 'first name': 'firstName', 'last name': 'lastName'}

    @staticmethod
    def get_key():
        return 'e-mail address'
