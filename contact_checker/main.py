#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

from gui.mainframe import MainFrame
from tools.preferences import Preferences


class Main:
    def __init__(self):
        self.preferences = Preferences(self.prepare_pref_file())
        self.run()

    def run(self):
        MainFrame(self)

    @staticmethod
    def prepare_pref_file():
        directory = os.path.join(os.getcwd(), 'data')
        filename = os.path.join(directory, 'preferences.json')

        if not os.path.isdir(directory):
            os.mkdir(directory)
        return filename


if __name__ == '__main__':
    main = Main()
