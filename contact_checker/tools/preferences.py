#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json


class Preferences:

    def __init__(self, filename):
        self.filename = filename

    def get_pref(self, key):
        d = self.load_preferences()
        try:
            return d[key]
        except KeyError:
            return None

    def set_pref(self, key, value):
        d = self.load_preferences()
        d[key] = value
        self.save_preferences(d)

    def load_preferences(self):
        try:
            with open(self.filename, 'r') as f:
                return json.load(f)
        except (IOError, ValueError):
            return {}

    def save_preferences(self, d):
        try:
            with open(self.filename, 'w') as f:
                text = json.dumps(d,  sort_keys=True, indent=4, separators=(',', ': '))
                f.write(text)
        except (IOError, ValueError):
            raise RuntimeError(u'Fehler beim Speichern der Preferences.')
