#!/usr/bin/env python
# -*- coding: utf-8 -*-
import wx

from contact_checker.gui.generate import gui

# ----------------------------------------------------------------------


class AboutDialog(gui.AboutDialog):

    def OnInitDialog(self, event):
        self.m_buttonOK.SetFocus()

# ----------------------------------------------------------------------


class FullContactKeyDialog(gui.FullContactKeyDialog):

    @property
    def key(self):
        return self.m_textCtrl_fullcontact_key.Value

    @key.setter
    def key(self, value):
        self.m_textCtrl_fullcontact_key.Value = value

    def __init__(self, parent, key):
        gui.FullContactKeyDialog.__init__(self, parent)
        self.key = key if key is not None else ''

    def OnInitDialog(self, event):
        self.m_textCtrl_fullcontact_key.SetFocus()

    def OnKeyDown(self, event):
        if event.GetKeyCode() == wx.WXK_RETURN:
            self.EndModal(wx.ID_OK)
        event.Skip()


class LimitDialog(gui.LimitDialog):

    @property
    def limit(self):
        return self.m_textCtrl_limit.Value

    @limit.setter
    def limit(self, value):
        self.m_textCtrl_limit.Value = value


# ----------------------------------------------------------------------


class LoginDialog(gui.LoginDialog):

    @property
    def username(self):
        return self.m_textCtrl_email.Value

    @property
    def password(self):
        return self.m_textCtrl_password.Value

    def on_init(self, event):
        self.m_textCtrl_email.SetFocus()

    def on_enter_email(self, event):
        if event.GetKeyCode() == wx.WXK_RETURN:
            self.m_textCtrl_email.SetFocus()
        event.Skip()

    def on_enter_password(self, event):
        if event.GetKeyCode() == wx.WXK_RETURN:
            self.EndModal(wx.ID_OK)
        event.Skip()


class XingMemberSearchDlg(gui.XingMemberSearchDlg):

    @property
    def search_tag(self):
        return self.m_textCtrl_seach_tag.Value

    @property
    def haves(self):
        return self.m_textCtrl_haves.Value

    @property
    def wants(self):
        return self.m_textCtrl_wants.Value

    @property
    def limit(self):
        return self.m_textCtrl_limit.Value

    @property
    def limit_groups(self):
        return self.m_textCtrl_limit_groups.Value

    @property
    def group_id(self):
        return self.m_textCtrl_search_groupid.Value

    @property
    def group_search(self):
        if self.m_notebook2.CurrentPage == self.m_panel_search_group:
            return True
        else:
            return False

    def onCancel(self, event):
        self.EndModal(wx.ID_CANCEL)

    def onOK(self, event):
        self.EndModal(wx.ID_OK)


class InvitationListDialog(gui.InvitationListDialog):

    @property
    def invitation_list(self):
        return self.m_textCtrl_invitation_list.Value

    @invitation_list.setter
    def invitation_list(self, value):
        self.m_textCtrl_invitation_list.Value = value


class InvitationDialog(gui.InvitationDialog):

    @property
    def subject(self):
        return self.m_textCtrl_subject.Value

    @property
    def message(self):
        return self.m_textCtrl_message.Value
