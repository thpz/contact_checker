#!/usr/bin/env python
# -*- coding: utf-8 -*-
import wx

import contact_checker.gui.generate.gui
from threading import Thread


EVT_UPDATE_ID = wx.NewId()
EVT_RESULT_ID = wx.NewId()


def evt_update(win, func):
    win.Connect(-1, -1, EVT_UPDATE_ID, func)


def evt_result(win, func):
    win.Connect(-1, -1, EVT_RESULT_ID, func)


class UpdateEvent(wx.PyEvent):

    def __init__(self, data):
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_UPDATE_ID)
        self.data = data


class ResultEvent(wx.PyEvent):

    def __init__(self, data):
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_RESULT_ID)
        self.data = data


class Updater:

    def __init__(self, parent):
        self.dlg = parent.waiting_dialog
        self.parent = parent
        self.step = 0
        self.all_steps = 0
        self.msg = ''

    def run(self, step=None, all_steps=None, msg=None):
        if step:
            self.step = step
        if all_steps:
            self.all_steps = all_steps
        if msg:
            self.msg = msg
        wx.PostEvent(self.dlg, UpdateEvent(self))

    def set_final_message(self, msg):
        self.parent.worker.message = dict(typ='Info', msg=msg)


class WaitingDialog(contact_checker.gui.generate.gui.WaitingDialog):

    def __init__(self, parent):
        contact_checker.gui.generate.gui.WaitingDialog.__init__(self, parent)

    def on_update(self, event):
        self.m_staticText_process.Label = 'Schritt {} von {}'.format(event.data.step, event.data.all_steps)
        self.m_textCtrl_process.Value = event.data.msg

    def on_tick(self, event):
        if self.m_gauge_process.Range <= self.m_gauge_process.Value:
            self.m_gauge_process.Value = 0
        self.m_gauge_process.Value += 10


class WaitDlg:

    def __init__(self, main_dlg):
        self.main_dlg = main_dlg
        evt_result(main_dlg, self.on_result)
        self.worker = None
        self.waiting_dialog = None

    def run(self, function, *args):
        wx.Yield()
        self.waiting_dialog = WaitingDialog(self.main_dlg)
        evt_update(self.waiting_dialog, self.waiting_dialog.on_update)
        self.worker = WorkerThread(self, function, *args)
        self.waiting_dialog.m_timer.Start(100)
        self.waiting_dialog.ShowModal()
        self.worker.stopped = True          # Krücke: Thread lebt weiter, aber pausiert
        self.worker = None
        self.waiting_dialog = None

    def on_result(self, event):
        self.waiting_dialog.m_timer.Stop()
        self.waiting_dialog.EndModal(wx.ID_CANCEL)

        if event.data['typ'] == 'Info':
            title = 'Information'
            icon = wx.ICON_INFORMATION
        else:
            title = 'Fehler'
            icon = wx.ICON_ERROR
        wx.MessageBox(event.data['msg'], title, wx.OK | icon)


class WorkerThread(Thread):

    def __init__(self, parent, function, *args):
        Thread.__init__(self)
        self.parent = parent
        self.function = function
        self.args = args
        self.message = dict(typ='Info', msg='Verarbeitung erfolgreich')
        self.start()

    def run(self):
        try:
            updater = Updater(self.parent)
            self.function(updater, *self.args)
        except Exception, e:
            self.message = dict(typ='Error', msg=e.message)
        finally:
            wx.PostEvent(self.parent.main_dlg, ResultEvent(self.message))
