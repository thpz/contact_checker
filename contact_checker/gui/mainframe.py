#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import wx

from contact_checker.controller import Controller
from contact_checker.gui.generate import gui
from contact_checker.gui.dialogs import AboutDialog, FullContactKeyDialog, LimitDialog, InvitationDialog,\
    InvitationListDialog, LoginDialog, XingMemberSearchDlg
from contact_checker.gui.waitdlg import WaitDlg
from contact_checker.xing_controller import XingController

# Einrichtung des virtual environments für wx
#   cd <env>/lib/python-2.7/site-packages
#   ln -s /usr/lib/python2.7/dist-packages/wx-2.8-gtk2-unicode/ .
#   ln -s /usr/lib/python2.7/dist-packages/wx.pth .
#   ln -s /usr/lib/python2.7/dist-packages/wxversion.py .
#   ln -s /usr/lib/python2.7/dist-packages/wxversion.pyc .


class MainFrame(gui.MainFrame):

    def __init__(self, main):
        self.app = wx.App()
        gui.MainFrame.__init__(self, None)

        self.prefs = main.preferences
        self.xing_controller = None
        database = self.prefs.get_pref(u'database')
        if database and os.path.isfile(database):
            self.call_controller(Controller.connect2database, database)

        self.wait_dlg = WaitDlg(self)

        self.run()

    def run(self):
        self.Show()
        self.app.MainLoop()

    # GENERAL STUFF

    def open_database(self, event):
        filename = self.prefs.get_pref(u'database')
        dirname = os.path.dirname(filename)\
            if filename is not None and os.path.isfile(filename)\
            else os.getcwd()
        basename = os.path.basename(filename)\
            if filename is not None and os.path.isfile(filename)\
            else 'database.sqlite3'

        open_file_dialog = wx.FileDialog(self, u'Datenbank öffnen', dirname, basename,
                                         u'Sqlite3-Datenbank (*.sqlite3)|*.sqlite3',
                                         wx.FD_OPEN)

        if open_file_dialog.ShowModal() == wx.ID_OK:
            filename = open_file_dialog.GetPath()
            open_file_dialog.Destroy()
            self.call_controller(Controller.connect2database, filename)
            self.prefs.set_pref(u'database', filename)

    def show_about_dialog(self, event):
        AboutDialog(self).ShowModal()

    def export_all(self, event):
        wx.MessageBox(u'Funktion noch nicht umgesetzt!', u'Fehler', wx.OK | wx.ICON_ERROR)

    def close_app(self, event):
        self.Close()

    # LINKEDIN STUFF

    def enter_fullcontact_key(self, event):
        full_contact_key = self.prefs.get_pref(u'full_contact_key')
        dialog = FullContactKeyDialog(self, full_contact_key)
        if dialog.ShowModal() == wx.ID_OK:
            self.prefs.set_pref(u'full_contact_key', dialog.m_textCtrl_fullcontact_key.Value)

    def new_contactlist(self, event):
        open_file_dialog = wx.FileDialog(self, u'Adressliste öffnen', '', '',
                                         u'Exceldateien (*.xlsx)|*.xlsx',
                                         wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)

        if open_file_dialog.ShowModal() == wx.ID_OK:
            open_file_dialog.Destroy()
            self.wait_dlg.run(Controller.load_new_contacts_from_excel, open_file_dialog.GetPath())
            self.update_stats()

    def linkedin_group(self, event):
        open_file_dialog = wx.FileDialog(self, u'LinkedIn-Gruppe öffnen', '', '',
                                         u'Exceldateien (*.xlsx)|*.xlsx',
                                         wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)

        if open_file_dialog.ShowModal() == wx.ID_OK:
            open_file_dialog.Destroy()
            self.wait_dlg.run(Controller.load_known_contacts_from_excel, open_file_dialog.GetPath())
            self.update_stats()

    def linkedin_check(self, event):
        limit_dialog = LimitDialog(self)
        limit_dialog.limit = self.m_textCtrl_linkedin_open.Value
        if limit_dialog.ShowModal() == wx.ID_OK:
            full_contact_key = self.prefs.get_pref(u'full_contact_key')
            limit = limit_dialog.limit
            self.wait_dlg.run(Controller.check_linkedin_via_fullcontact, full_contact_key, limit)
            self.update_stats()

    def package_export(self, event):
        open_dir_dialog = wx.DirDialog(self, u'Export-Pfad wählen')

        if open_dir_dialog.ShowModal() != wx.ID_OK:
            return

        export_path = open_dir_dialog.GetPath()
        self.wait_dlg.run(Controller.export_linkedin_invitations, export_path)
        self.update_stats()

    def update_stats(self):
        stats = Controller.get_statistics()
        self.m_textCtrl_all_contacts.Value = str(stats.contacts)
        self.m_textCtrl_linkedin.Value = str(stats.in_linkedin_group)
        self.m_textCtrl_not_linkedin.Value = str(stats.not_in_linkedin_group)
        self.m_textCtrl_linkedin_open.Value = str(stats.linkedin_unchecked)
        self.m_textCtrl_has_linkedin.Value = str(stats.with_linkedin_account)
        self.m_textCtrl_has__not_linkedin.Value = str(stats.without_linkedin_account)
        self.m_textCtrl_linkedin_invited.Value = str(stats.invited_to_linkedin_group)
        self.m_textCtrl_linkedin_not_invited.Value = str(stats.not_invited_to_linkedin_group)

    # XING STUFF

    def xing_login(self, event):
        login_dialog = LoginDialog(self)
        if login_dialog.ShowModal() == wx.ID_OK:
            self.xing_controller = XingController()
            self.wait_dlg.run(self.xing_controller.perform_login, login_dialog.username, login_dialog.password)
            self.m_textCtrl_xing_username.Value = self.xing_controller.username

            if self.xing_controller.username is not None:
                self.m_button_xing_search_members.Enabled = True
                self.m_button_xing_invite.Enabled = True
                self.m_button_xing_clear_workingtable.Enabled = True

            self.m_choice_xing_groups.Clear()
            if self.xing_controller.groups:
                for g in self.xing_controller.groups:
                    self.m_choice_xing_groups.Append(g[1], g)
                self.m_choice_xing_groups.SetSelection(0)
                self.on_choice(None)

        # ToDo ZUM TEST EINKOMMENTIEREN

            self.update_xing_stats()
        else:
            self.xing_controller = XingController()
            self.m_choice_xing_groups.Clear()
            self.m_choice_xing_groups.Append("Draht und Kabel", [1068316, "Draht und Kabel"])
            self.m_choice_xing_groups.Append("Ariane", ["Ariane", "Paetz"])
            self.m_choice_xing_groups.Append("Technik-Kommunikation RWTH Aachen",
                                             [1059075, "Technik-Kommunikation RWTH Aachen"])
            self.m_choice_xing_groups.Append("Porsche Club 928",
                                             [1025418, "Porsche Club 928"])
            self.m_button_xing_search_members.Enabled = True
            self.m_button_xing_invite.Enabled = True
            self.m_button_xing_clear_workingtable.Enabled = True

    def on_choice(self, event):
        selected_id = self.m_choice_xing_groups.GetSelection()
        self.xing_controller.selected_group = self.m_choice_xing_groups.GetClientData(selected_id)
        self.update_xing_stats()

    def clear_workingtable(self, event):
        g = self.xing_controller.selected_group
        dlg = wx.MessageDialog(self,
                               u'Soll die Working Table für die Gruppe "{}" wirklich gelöscht werden?'.format(g[1]),
                               u'Working Table löschen', wx.YES_NO | wx.ICON_QUESTION)
        result = dlg.ShowModal()
        dlg.Destroy()
        if result == wx.ID_YES:
            try:
                self.xing_controller.clear_working_table()
                wx.MessageBox(u'Verarbeitung erfolgreich', u'Information', wx.OK | wx.ICON_INFORMATION)
            except Exception, e:
                message = e.message
                wx.MessageBox(message, u'Fehler', wx.OK | wx.ICON_ERROR)
            finally:
                self.update_xing_stats()

    def search_members(self, event):
        dlg = XingMemberSearchDlg(self)
        if dlg.ShowModal() == wx.ID_OK:
            if dlg.group_search:
                group_id = dlg.group_id.encode('utf-8')
                limit = dlg.limit_groups.encode('utf-8')
                self.wait_dlg.run(self.xing_controller.search_group_members, group_id, limit)
            else:
                search_tag = dlg.search_tag.encode('utf-8')
                haves = dlg.haves.encode('utf-8')
                wants = dlg.wants.encode('utf-8')
                limit = dlg.limit.encode('utf-8')
                self.wait_dlg.run(self.xing_controller.search_members, search_tag, wants, haves, limit)

            self.update_xing_stats()

    def send_invitations(self, event):
        uninvited = u''
        for user in self.xing_controller.get_uninvited():
            uninvited += u'{} ({})\n'.format(user[1], user[0])

        dlg = InvitationListDialog(self)
        dlg.invitation_list = uninvited
        if dlg.ShowModal() == wx.ID_OK:
            limit = dlg.m_textCtrl_inv_limit.Value
            inv_per_min = dlg.m_textCtrl_inv_per_min.Value
            dlg2 = InvitationDialog(self)
            if dlg2.ShowModal() == wx.ID_OK:
                self.wait_dlg.run(self.xing_controller.send_invitations,
                                  dlg2.subject.encode('utf-8'),
                                  dlg2.message.encode('utf-8'),
                                  limit,
                                  inv_per_min)
                self.update_xing_stats()

    def update_xing_stats(self):
        if self.xing_controller is None or self.xing_controller.selected_group is None:
            return

        stats = self.xing_controller.get_statistics()
        self.m_textCtrl_xing_invited_members.Value = str(stats['xing_invited'])
        self.m_textCtrl_xing_members_working_table.Value = str(stats['xing_workingtable'])
        self.m_textCtrl_xing_members_not_invited.Value = str(stats['xing_not_invited'])
        self.m_textCtrl_last_invitation.Value =\
            str(stats['xing_last_invitation'])[:16] if stats['xing_last_invitation'] is not None else '--'

    # TOOLING STUFF

    def call_controller(self, function, *args):
        try:
            wx.Yield()
            function(*args)
        except Exception, e:
            message = e.message
            wx.MessageBox(message, u'Fehler', wx.OK | wx.ICON_ERROR)
        finally:
            self.update_stats()
            self.update_xing_stats()
