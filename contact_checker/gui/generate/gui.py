# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jan 11 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MainFrame
###########################################################################

class MainFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Contact Checker", pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		self.m_menubar = wx.MenuBar( 0 )
		self.m_menu_file = wx.Menu()
		self.m_menuItem_database = wx.MenuItem( self.m_menu_file, wx.ID_ANY, u"Datenbank auswählen", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu_file.AppendItem( self.m_menuItem_database )
		
		self.m_menuItem_fullconctact_key = wx.MenuItem( self.m_menu_file, wx.ID_ANY, u"Full Contact Key", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu_file.AppendItem( self.m_menuItem_fullconctact_key )
		
		self.m_menu_file.AppendSeparator()
		
		self.m_menuItem_close = wx.MenuItem( self.m_menu_file, wx.ID_ANY, u"Schließen", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu_file.AppendItem( self.m_menuItem_close )
		
		self.m_menubar.Append( self.m_menu_file, u"Datei" ) 
		
		self.m_menu_help = wx.Menu()
		self.m_menuItem_about = wx.MenuItem( self.m_menu_help, wx.ID_ANY, u"Über Contact Checker", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu_help.AppendItem( self.m_menuItem_about )
		
		self.m_menubar.Append( self.m_menu_help, u"Hilfe" ) 
		
		self.SetMenuBar( self.m_menubar )
		
		bSizer3 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_panel4 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer8 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_notebook1 = wx.Notebook( self.m_panel4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_panel1 = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer1 = wx.FlexGridSizer( 8, 3, 0, 0 )
		fgSizer1.SetFlexibleDirection( wx.BOTH )
		fgSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText4 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Kontakte insgesamt:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText4.Wrap( -1 )
		fgSizer1.Add( self.m_staticText4, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrl_all_contacts = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		fgSizer1.Add( self.m_textCtrl_all_contacts, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_button_new_contactlist = wx.Button( self.m_panel1, wx.ID_ANY, u"Adressliste einlesen", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer1.Add( self.m_button_new_contactlist, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.m_staticText5 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"   bereits in LinkedIn-Gruppe:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )
		fgSizer1.Add( self.m_staticText5, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrl_linkedin = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		fgSizer1.Add( self.m_textCtrl_linkedin, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_button_linkedin_group = wx.Button( self.m_panel1, wx.ID_ANY, u"LinkedIn-Gruppe einlesen", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer1.Add( self.m_button_linkedin_group, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.m_staticText6 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"   noch nicht in LinkedIn-Gruppe:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText6.Wrap( -1 )
		fgSizer1.Add( self.m_staticText6, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrl_not_linkedin = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		fgSizer1.Add( self.m_textCtrl_not_linkedin, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_staticText35 = wx.StaticText( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText35.Wrap( -1 )
		fgSizer1.Add( self.m_staticText35, 0, wx.ALL, 5 )
		
		self.m_staticText7 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"      LinkedIn ungeprüft:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText7.Wrap( -1 )
		fgSizer1.Add( self.m_staticText7, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrl_linkedin_open = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		fgSizer1.Add( self.m_textCtrl_linkedin_open, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_button_linkedin_check = wx.Button( self.m_panel1, wx.ID_ANY, u"Jetzt prüfen!", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer1.Add( self.m_button_linkedin_check, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.m_staticText8 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"      ohne LinkedIn Account:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText8.Wrap( -1 )
		fgSizer1.Add( self.m_staticText8, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrl_has__not_linkedin = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		fgSizer1.Add( self.m_textCtrl_has__not_linkedin, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_staticText36 = wx.StaticText( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText36.Wrap( -1 )
		fgSizer1.Add( self.m_staticText36, 0, wx.ALL, 5 )
		
		self.m_staticText9 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"      mit LinkedIn Account:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText9.Wrap( -1 )
		fgSizer1.Add( self.m_staticText9, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrl_has_linkedin = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		fgSizer1.Add( self.m_textCtrl_has_linkedin, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_staticText37 = wx.StaticText( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText37.Wrap( -1 )
		fgSizer1.Add( self.m_staticText37, 0, wx.ALL, 5 )
		
		self.m_staticText10 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"         Einladung bekommen:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText10.Wrap( -1 )
		fgSizer1.Add( self.m_staticText10, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrl_linkedin_invited = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		fgSizer1.Add( self.m_textCtrl_linkedin_invited, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_staticText38 = wx.StaticText( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText38.Wrap( -1 )
		fgSizer1.Add( self.m_staticText38, 0, wx.ALL, 5 )
		
		self.m_staticText11 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"         noch nicht eingeladen:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText11.Wrap( -1 )
		fgSizer1.Add( self.m_staticText11, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrl_linkedin_not_invited = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		fgSizer1.Add( self.m_textCtrl_linkedin_not_invited, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_button_package_export = wx.Button( self.m_panel1, wx.ID_ANY, u"Pakete exportieren", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer1.Add( self.m_button_package_export, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		
		self.m_panel1.SetSizer( fgSizer1 )
		self.m_panel1.Layout()
		fgSizer1.Fit( self.m_panel1 )
		self.m_notebook1.AddPage( self.m_panel1, u"LinkedIn", False )
		self.m_panel5 = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer14 = wx.BoxSizer( wx.VERTICAL )
		
		fgSizer2 = wx.FlexGridSizer( 2, 2, 0, 0 )
		fgSizer2.AddGrowableCol( 1 )
		fgSizer2.AddGrowableRow( 0 )
		fgSizer2.AddGrowableRow( 1 )
		fgSizer2.SetFlexibleDirection( wx.BOTH )
		fgSizer2.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText14 = wx.StaticText( self.m_panel5, wx.ID_ANY, u"User:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText14.Wrap( -1 )
		fgSizer2.Add( self.m_staticText14, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_panel6 = wx.Panel( self.m_panel5, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer13 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_textCtrl_xing_username = wx.TextCtrl( self.m_panel6, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer13.Add( self.m_textCtrl_xing_username, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.m_button_xing_login = wx.Button( self.m_panel6, wx.ID_ANY, u"Login...", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer13.Add( self.m_button_xing_login, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		self.m_panel6.SetSizer( bSizer13 )
		self.m_panel6.Layout()
		bSizer13.Fit( self.m_panel6 )
		fgSizer2.Add( self.m_panel6, 1, wx.EXPAND |wx.ALL, 0 )
		
		self.m_staticText15 = wx.StaticText( self.m_panel5, wx.ID_ANY, u"Gruppe:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText15.Wrap( -1 )
		fgSizer2.Add( self.m_staticText15, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_panel11 = wx.Panel( self.m_panel5, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer22 = wx.BoxSizer( wx.VERTICAL )
		
		m_choice_xing_groupsChoices = [ u"Gruppen..." ]
		self.m_choice_xing_groups = wx.Choice( self.m_panel11, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice_xing_groupsChoices, 0 )
		self.m_choice_xing_groups.SetSelection( 0 )
		bSizer22.Add( self.m_choice_xing_groups, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		self.m_panel11.SetSizer( bSizer22 )
		self.m_panel11.Layout()
		bSizer22.Fit( self.m_panel11 )
		fgSizer2.Add( self.m_panel11, 1, wx.EXPAND |wx.ALL, 0 )
		
		
		bSizer14.Add( fgSizer2, 1, wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND, 0 )
		
		fgSizer5 = wx.FlexGridSizer( 0, 2, 0, 0 )
		fgSizer5.SetFlexibleDirection( wx.BOTH )
		fgSizer5.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText20 = wx.StaticText( self.m_panel5, wx.ID_ANY, u"XING-Mitglieder eingeladen:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText20.Wrap( -1 )
		fgSizer5.Add( self.m_staticText20, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.m_textCtrl_xing_invited_members = wx.TextCtrl( self.m_panel5, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		fgSizer5.Add( self.m_textCtrl_xing_invited_members, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText18 = wx.StaticText( self.m_panel5, wx.ID_ANY, u"XING-Mitglieder in Working Table:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText18.Wrap( -1 )
		fgSizer5.Add( self.m_staticText18, 0, wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrl_xing_members_working_table = wx.TextCtrl( self.m_panel5, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		fgSizer5.Add( self.m_textCtrl_xing_members_working_table, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.m_staticText19 = wx.StaticText( self.m_panel5, wx.ID_ANY, u"davon noch nicht eingeladen:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText19.Wrap( -1 )
		fgSizer5.Add( self.m_staticText19, 0, wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrl_xing_members_not_invited = wx.TextCtrl( self.m_panel5, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		fgSizer5.Add( self.m_textCtrl_xing_members_not_invited, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.m_staticText21 = wx.StaticText( self.m_panel5, wx.ID_ANY, u"Letzte Einladungen am:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText21.Wrap( -1 )
		fgSizer5.Add( self.m_staticText21, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.m_textCtrl_last_invitation = wx.TextCtrl( self.m_panel5, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.m_textCtrl_last_invitation.SetMinSize( wx.Size( 200,-1 ) )
		
		fgSizer5.Add( self.m_textCtrl_last_invitation, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		
		bSizer14.Add( fgSizer5, 0, wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		bSizer15 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_button_xing_search_members = wx.Button( self.m_panel5, wx.ID_ANY, u"Mitglieder suchen...", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_button_xing_search_members.Enable( False )
		
		bSizer15.Add( self.m_button_xing_search_members, 0, wx.ALL, 5 )
		
		self.m_button_xing_invite = wx.Button( self.m_panel5, wx.ID_ANY, u"Einladungen versenden...", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_button_xing_invite.Enable( False )
		
		bSizer15.Add( self.m_button_xing_invite, 0, wx.ALL, 5 )
		
		self.m_button_xing_clear_workingtable = wx.Button( self.m_panel5, wx.ID_ANY, u"Working Table löschen...", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_button_xing_clear_workingtable.Enable( False )
		
		bSizer15.Add( self.m_button_xing_clear_workingtable, 0, wx.ALL, 5 )
		
		
		bSizer14.Add( bSizer15, 1, wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		self.m_panel5.SetSizer( bSizer14 )
		self.m_panel5.Layout()
		bSizer14.Fit( self.m_panel5 )
		self.m_notebook1.AddPage( self.m_panel5, u"XING", True )
		
		bSizer8.Add( self.m_notebook1, 1, wx.EXPAND |wx.ALL, 5 )
		
		self.m_panel3 = wx.Panel( self.m_panel4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer9 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_button_export_all = wx.Button( self.m_panel3, wx.ID_ANY, u"Alle Kontakte exportieren", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer9.Add( self.m_button_export_all, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_button_close = wx.Button( self.m_panel3, wx.ID_ANY, u"Schließen", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer9.Add( self.m_button_close, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		self.m_panel3.SetSizer( bSizer9 )
		self.m_panel3.Layout()
		bSizer9.Fit( self.m_panel3 )
		bSizer8.Add( self.m_panel3, 0, wx.EXPAND|wx.ALL, 5 )
		
		
		self.m_panel4.SetSizer( bSizer8 )
		self.m_panel4.Layout()
		bSizer8.Fit( self.m_panel4 )
		bSizer3.Add( self.m_panel4, 1, wx.EXPAND |wx.ALL, 0 )
		
		
		self.SetSizer( bSizer3 )
		self.Layout()
		bSizer3.Fit( self )
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.Bind( wx.EVT_MENU, self.open_database, id = self.m_menuItem_database.GetId() )
		self.Bind( wx.EVT_MENU, self.enter_fullcontact_key, id = self.m_menuItem_fullconctact_key.GetId() )
		self.Bind( wx.EVT_MENU, self.close_app, id = self.m_menuItem_close.GetId() )
		self.Bind( wx.EVT_MENU, self.show_about_dialog, id = self.m_menuItem_about.GetId() )
		self.m_button_new_contactlist.Bind( wx.EVT_BUTTON, self.new_contactlist )
		self.m_button_linkedin_group.Bind( wx.EVT_BUTTON, self.linkedin_group )
		self.m_button_linkedin_check.Bind( wx.EVT_BUTTON, self.linkedin_check )
		self.m_button_package_export.Bind( wx.EVT_BUTTON, self.package_export )
		self.m_button_xing_login.Bind( wx.EVT_BUTTON, self.xing_login )
		self.m_choice_xing_groups.Bind( wx.EVT_CHOICE, self.on_choice )
		self.m_button_xing_search_members.Bind( wx.EVT_BUTTON, self.search_members )
		self.m_button_xing_invite.Bind( wx.EVT_BUTTON, self.send_invitations )
		self.m_button_xing_clear_workingtable.Bind( wx.EVT_BUTTON, self.clear_workingtable )
		self.m_button_export_all.Bind( wx.EVT_BUTTON, self.export_all )
		self.m_button_close.Bind( wx.EVT_BUTTON, self.close_app )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def open_database( self, event ):
		event.Skip()
	
	def enter_fullcontact_key( self, event ):
		event.Skip()
	
	def close_app( self, event ):
		event.Skip()
	
	def show_about_dialog( self, event ):
		event.Skip()
	
	def new_contactlist( self, event ):
		event.Skip()
	
	def linkedin_group( self, event ):
		event.Skip()
	
	def linkedin_check( self, event ):
		event.Skip()
	
	def package_export( self, event ):
		event.Skip()
	
	def xing_login( self, event ):
		event.Skip()
	
	def on_choice( self, event ):
		event.Skip()
	
	def search_members( self, event ):
		event.Skip()
	
	def send_invitations( self, event ):
		event.Skip()
	
	def clear_workingtable( self, event ):
		event.Skip()
	
	def export_all( self, event ):
		event.Skip()
	
	

###########################################################################
## Class InvitationListDialog
###########################################################################

class InvitationListDialog ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Einladungen versenden", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer18 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_panel9 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer19 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText26 = wx.StaticText( self.m_panel9, wx.ID_ANY, u"Eingeladen werden (Name, XING-ID):", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText26.Wrap( -1 )
		bSizer19.Add( self.m_staticText26, 0, wx.ALL, 5 )
		
		self.m_textCtrl_invitation_list = wx.TextCtrl( self.m_panel9, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY )
		self.m_textCtrl_invitation_list.SetMinSize( wx.Size( 350,400 ) )
		
		bSizer19.Add( self.m_textCtrl_invitation_list, 0, wx.ALL, 5 )
		
		fgSizer6 = wx.FlexGridSizer( 0, 2, 0, 0 )
		fgSizer6.SetFlexibleDirection( wx.BOTH )
		fgSizer6.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText30 = wx.StaticText( self.m_panel9, wx.ID_ANY, u"Anzahl Einladungen (0 = alle):", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText30.Wrap( -1 )
		fgSizer6.Add( self.m_staticText30, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrl_inv_limit = wx.TextCtrl( self.m_panel9, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer6.Add( self.m_textCtrl_inv_limit, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.m_staticText31 = wx.StaticText( self.m_panel9, wx.ID_ANY, u"Einladungen / Stunde (Default 60):", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText31.Wrap( -1 )
		fgSizer6.Add( self.m_staticText31, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrl_inv_per_min = wx.TextCtrl( self.m_panel9, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer6.Add( self.m_textCtrl_inv_per_min, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		bSizer19.Add( fgSizer6, 0, wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		m_sdbSizer6 = wx.StdDialogButtonSizer()
		self.m_sdbSizer6OK = wx.Button( self.m_panel9, wx.ID_OK )
		m_sdbSizer6.AddButton( self.m_sdbSizer6OK )
		self.m_sdbSizer6Cancel = wx.Button( self.m_panel9, wx.ID_CANCEL )
		m_sdbSizer6.AddButton( self.m_sdbSizer6Cancel )
		m_sdbSizer6.Realize();
		
		bSizer19.Add( m_sdbSizer6, 1, wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		self.m_panel9.SetSizer( bSizer19 )
		self.m_panel9.Layout()
		bSizer19.Fit( self.m_panel9 )
		bSizer18.Add( self.m_panel9, 1, wx.EXPAND |wx.ALL, 5 )
		
		
		self.SetSizer( bSizer18 )
		self.Layout()
		bSizer18.Fit( self )
		
		self.Centre( wx.BOTH )
	
	def __del__( self ):
		pass
	

###########################################################################
## Class WaitingDialog
###########################################################################

class WaitingDialog ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Verarbeitung läuft...", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.CLOSE_BOX|wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer6 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer7 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText_process = wx.StaticText( self, wx.ID_ANY, u"Schritt 0 von 0:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText_process.Wrap( -1 )
		bSizer7.Add( self.m_staticText_process, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrl_process = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.m_textCtrl_process.SetMinSize( wx.Size( 250,-1 ) )
		
		bSizer7.Add( self.m_textCtrl_process, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		
		bSizer6.Add( bSizer7, 1, wx.EXPAND, 5 )
		
		bSizer8 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_gauge_process = wx.Gauge( self, wx.ID_ANY, 100, wx.DefaultPosition, wx.DefaultSize, wx.GA_HORIZONTAL )
		self.m_gauge_process.SetValue( 0 ) 
		bSizer8.Add( self.m_gauge_process, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		bSizer6.Add( bSizer8, 1, wx.EXPAND, 5 )
		
		m_sdbSizer4 = wx.StdDialogButtonSizer()
		self.m_sdbSizer4Cancel = wx.Button( self, wx.ID_CANCEL )
		m_sdbSizer4.AddButton( self.m_sdbSizer4Cancel )
		m_sdbSizer4.Realize();
		
		bSizer6.Add( m_sdbSizer4, 1, wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		self.SetSizer( bSizer6 )
		self.Layout()
		bSizer6.Fit( self )
		self.m_timer = wx.Timer()
		self.m_timer.SetOwner( self, wx.ID_ANY )
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.Bind( wx.EVT_TIMER, self.on_tick, id=wx.ID_ANY )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def on_tick( self, event ):
		event.Skip()
	

###########################################################################
## Class FullContactKeyDialog
###########################################################################

class FullContactKeyDialog ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Full Contact Key", pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer10 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_panel4 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer11 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText12 = wx.StaticText( self.m_panel4, wx.ID_ANY, u"Full Contact Key:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText12.Wrap( -1 )
		bSizer11.Add( self.m_staticText12, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrl_fullcontact_key = wx.TextCtrl( self.m_panel4, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textCtrl_fullcontact_key.SetMaxLength( 30 ) 
		self.m_textCtrl_fullcontact_key.SetMinSize( wx.Size( 200,-1 ) )
		
		bSizer11.Add( self.m_textCtrl_fullcontact_key, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		
		self.m_panel4.SetSizer( bSizer11 )
		self.m_panel4.Layout()
		bSizer11.Fit( self.m_panel4 )
		bSizer10.Add( self.m_panel4, 0, wx.ALL|wx.EXPAND, 5 )
		
		m_sdbSizer1 = wx.StdDialogButtonSizer()
		self.m_sdbSizer1OK = wx.Button( self, wx.ID_OK )
		m_sdbSizer1.AddButton( self.m_sdbSizer1OK )
		self.m_sdbSizer1Cancel = wx.Button( self, wx.ID_CANCEL )
		m_sdbSizer1.AddButton( self.m_sdbSizer1Cancel )
		m_sdbSizer1.Realize();
		
		bSizer10.Add( m_sdbSizer1, 0, wx.EXPAND, 5 )
		
		
		self.SetSizer( bSizer10 )
		self.Layout()
		bSizer10.Fit( self )
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.Bind( wx.EVT_INIT_DIALOG, self.OnInitDialog )
		self.m_textCtrl_fullcontact_key.Bind( wx.EVT_KEY_DOWN, self.OnKeyDown )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def OnInitDialog( self, event ):
		event.Skip()
	
	def OnKeyDown( self, event ):
		event.Skip()
	

###########################################################################
## Class LimitDialog
###########################################################################

class LimitDialog ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Limit", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer9 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer10 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText11 = wx.StaticText( self, wx.ID_ANY, u"Limit:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText11.Wrap( -1 )
		bSizer10.Add( self.m_staticText11, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrl_limit = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer10.Add( self.m_textCtrl_limit, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		
		bSizer9.Add( bSizer10, 1, wx.EXPAND, 5 )
		
		m_sdbSizer3 = wx.StdDialogButtonSizer()
		self.m_sdbSizer3OK = wx.Button( self, wx.ID_OK )
		m_sdbSizer3.AddButton( self.m_sdbSizer3OK )
		self.m_sdbSizer3Cancel = wx.Button( self, wx.ID_CANCEL )
		m_sdbSizer3.AddButton( self.m_sdbSizer3Cancel )
		m_sdbSizer3.Realize();
		
		bSizer9.Add( m_sdbSizer3, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( bSizer9 )
		self.Layout()
		bSizer9.Fit( self )
		
		self.Centre( wx.BOTH )
	
	def __del__( self ):
		pass
	

###########################################################################
## Class AboutDialog
###########################################################################

class AboutDialog ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Über Contact Checker", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer11 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText12 = wx.StaticText( self, wx.ID_ANY, u"*** CONTACT CHECKER ***", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText12.Wrap( -1 )
		self.m_staticText12.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer11.Add( self.m_staticText12, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.m_staticText13 = wx.StaticText( self, wx.ID_ANY, u"Version: 1.0\n\nDie Benutzung des Programms geschieht\nauf eigene Verantwortung.", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText13.Wrap( -1 )
		bSizer11.Add( self.m_staticText13, 0, wx.ALL, 5 )
		
		m_button = wx.StdDialogButtonSizer()
		self.m_buttonOK = wx.Button( self, wx.ID_OK )
		m_button.AddButton( self.m_buttonOK )
		m_button.Realize();
		
		bSizer11.Add( m_button, 0, wx.ALIGN_CENTER, 5 )
		
		
		self.SetSizer( bSizer11 )
		self.Layout()
		bSizer11.Fit( self )
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.Bind( wx.EVT_INIT_DIALOG, self.OnInitDialog )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def OnInitDialog( self, event ):
		event.Skip()
	

###########################################################################
## Class LoginDialog
###########################################################################

class LoginDialog ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"XING Login", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer12 = wx.BoxSizer( wx.VERTICAL )
		
		fgSizer3 = wx.FlexGridSizer( 0, 2, 0, 0 )
		fgSizer3.SetFlexibleDirection( wx.BOTH )
		fgSizer3.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText18 = wx.StaticText( self, wx.ID_ANY, u"E-Mail:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText18.Wrap( -1 )
		fgSizer3.Add( self.m_staticText18, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL|wx.ALIGN_RIGHT, 5 )
		
		self.m_textCtrl_email = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textCtrl_email.SetMinSize( wx.Size( 250,-1 ) )
		
		fgSizer3.Add( self.m_textCtrl_email, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_staticText19 = wx.StaticText( self, wx.ID_ANY, u"Passwort:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText19.Wrap( -1 )
		fgSizer3.Add( self.m_staticText19, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.m_textCtrl_password = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD )
		self.m_textCtrl_password.SetMinSize( wx.Size( 250,-1 ) )
		
		fgSizer3.Add( self.m_textCtrl_password, 0, wx.ALL, 5 )
		
		
		bSizer12.Add( fgSizer3, 1, wx.EXPAND, 5 )
		
		m_sdbSizer5 = wx.StdDialogButtonSizer()
		self.m_sdbSizer5OK = wx.Button( self, wx.ID_OK )
		m_sdbSizer5.AddButton( self.m_sdbSizer5OK )
		self.m_sdbSizer5Cancel = wx.Button( self, wx.ID_CANCEL )
		m_sdbSizer5.AddButton( self.m_sdbSizer5Cancel )
		m_sdbSizer5.Realize();
		
		bSizer12.Add( m_sdbSizer5, 0, wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		self.SetSizer( bSizer12 )
		self.Layout()
		bSizer12.Fit( self )
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.Bind( wx.EVT_INIT_DIALOG, self.on_init )
		self.m_textCtrl_email.Bind( wx.EVT_KEY_DOWN, self.on_enter_email )
		self.m_textCtrl_password.Bind( wx.EVT_KEY_DOWN, self.on_enter_password )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def on_init( self, event ):
		event.Skip()
	
	def on_enter_email( self, event ):
		event.Skip()
	
	def on_enter_password( self, event ):
		event.Skip()
	

###########################################################################
## Class XingMemberSearchDlg
###########################################################################

class XingMemberSearchDlg ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"XING-Mitglieder zur Working Table hinzufügen", pos = wx.DefaultPosition, size = wx.Size( 530,279 ), style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer16 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_panel7 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer23 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_notebook2 = wx.Notebook( self.m_panel7, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_panel_search_member = wx.Panel( self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer5 = wx.FlexGridSizer( 0, 2, 0, 0 )
		fgSizer5.SetFlexibleDirection( wx.BOTH )
		fgSizer5.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText22 = wx.StaticText( self.m_panel_search_member, wx.ID_ANY, u"Suchbegriff:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText22.Wrap( -1 )
		fgSizer5.Add( self.m_staticText22, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.m_textCtrl_seach_tag = wx.TextCtrl( self.m_panel_search_member, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textCtrl_seach_tag.SetMinSize( wx.Size( 400,-1 ) )
		
		fgSizer5.Add( self.m_textCtrl_seach_tag, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.m_staticText23 = wx.StaticText( self.m_panel_search_member, wx.ID_ANY, u"Ich biete:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText23.Wrap( -1 )
		fgSizer5.Add( self.m_staticText23, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.m_textCtrl_haves = wx.TextCtrl( self.m_panel_search_member, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer5.Add( self.m_textCtrl_haves, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.EXPAND, 5 )
		
		self.m_staticText24 = wx.StaticText( self.m_panel_search_member, wx.ID_ANY, u"Ich suche:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText24.Wrap( -1 )
		fgSizer5.Add( self.m_staticText24, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.m_textCtrl_wants = wx.TextCtrl( self.m_panel_search_member, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer5.Add( self.m_textCtrl_wants, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.m_staticText25 = wx.StaticText( self.m_panel_search_member, wx.ID_ANY, u"Limit:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText25.Wrap( -1 )
		fgSizer5.Add( self.m_staticText25, 0, wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrl_limit = wx.TextCtrl( self.m_panel_search_member, wx.ID_ANY, u"50", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer5.Add( self.m_textCtrl_limit, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		self.m_panel_search_member.SetSizer( fgSizer5 )
		self.m_panel_search_member.Layout()
		fgSizer5.Fit( self.m_panel_search_member )
		self.m_notebook2.AddPage( self.m_panel_search_member, u"Erweiterte XING-Suche", False )
		self.m_panel_search_group = wx.Panel( self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer51 = wx.FlexGridSizer( 0, 2, 0, 0 )
		fgSizer51.SetFlexibleDirection( wx.BOTH )
		fgSizer51.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText221 = wx.StaticText( self.m_panel_search_group, wx.ID_ANY, u"Gruppen ID:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText221.Wrap( -1 )
		fgSizer51.Add( self.m_staticText221, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.m_textCtrl_search_groupid = wx.TextCtrl( self.m_panel_search_group, wx.ID_ANY, u"clean-energy-xing-ambassador-community-bb68-1071144", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textCtrl_search_groupid.SetMinSize( wx.Size( 400,-1 ) )
		
		fgSizer51.Add( self.m_textCtrl_search_groupid, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.m_staticText39 = wx.StaticText( self.m_panel_search_group, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText39.Wrap( -1 )
		fgSizer51.Add( self.m_staticText39, 0, wx.ALL, 5 )
		
		self.m_staticText40 = wx.StaticText( self.m_panel_search_group, wx.ID_ANY, u"Gruppen ID aus URL kopieren, z.B. für Clean Energy:\nclean-energy-xing-ambassador-community-bb68-1071144", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText40.Wrap( -1 )
		fgSizer51.Add( self.m_staticText40, 0, wx.ALL, 5 )
		
		self.m_staticText251 = wx.StaticText( self.m_panel_search_group, wx.ID_ANY, u"Limit:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText251.Wrap( -1 )
		fgSizer51.Add( self.m_staticText251, 0, wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrl_limit_groups = wx.TextCtrl( self.m_panel_search_group, wx.ID_ANY, u"50", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer51.Add( self.m_textCtrl_limit_groups, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		self.m_panel_search_group.SetSizer( fgSizer51 )
		self.m_panel_search_group.Layout()
		fgSizer51.Fit( self.m_panel_search_group )
		self.m_notebook2.AddPage( self.m_panel_search_group, u"Gruppenmitglieder", True )
		
		bSizer23.Add( self.m_notebook2, 1, wx.EXPAND |wx.ALL, 5 )
		
		
		self.m_panel7.SetSizer( bSizer23 )
		self.m_panel7.Layout()
		bSizer23.Fit( self.m_panel7 )
		bSizer16.Add( self.m_panel7, 1, wx.EXPAND |wx.ALL, 5 )
		
		self.m_panel8 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer17 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_button11 = wx.Button( self.m_panel8, wx.ID_ANY, u"Hinzufügen", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer17.Add( self.m_button11, 0, wx.ALL, 5 )
		
		self.m_button12 = wx.Button( self.m_panel8, wx.ID_ANY, u"Abbrechen", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_button12.SetDefault() 
		bSizer17.Add( self.m_button12, 0, wx.ALL, 5 )
		
		
		self.m_panel8.SetSizer( bSizer17 )
		self.m_panel8.Layout()
		bSizer17.Fit( self.m_panel8 )
		bSizer16.Add( self.m_panel8, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		self.SetSizer( bSizer16 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.m_button11.Bind( wx.EVT_BUTTON, self.onOK )
		self.m_button12.Bind( wx.EVT_BUTTON, self.onCancel )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def onOK( self, event ):
		event.Skip()
	
	def onCancel( self, event ):
		event.Skip()
	

###########################################################################
## Class InvitationDialog
###########################################################################

class InvitationDialog ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Einladungen versenden", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer20 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_panel10 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer21 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText27 = wx.StaticText( self.m_panel10, wx.ID_ANY, u"Betreff:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText27.Wrap( -1 )
		bSizer21.Add( self.m_staticText27, 0, wx.ALL, 5 )
		
		self.m_textCtrl_subject = wx.TextCtrl( self.m_panel10, wx.ID_ANY, u"Einladung in die Gruppe \"...\"", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer21.Add( self.m_textCtrl_subject, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText28 = wx.StaticText( self.m_panel10, wx.ID_ANY, u"Nachricht (mind. 3 und max. 500 Zeichen erlaubt):", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText28.Wrap( -1 )
		bSizer21.Add( self.m_staticText28, 0, wx.ALL, 5 )
		
		self.m_staticText29 = wx.StaticText( self.m_panel10, wx.ID_ANY, u"Platzhalter: $$NAME$$", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText29.Wrap( -1 )
		bSizer21.Add( self.m_staticText29, 0, wx.ALL, 5 )
		
		self.m_textCtrl_message = wx.TextCtrl( self.m_panel10, wx.ID_ANY, u"Hallo $$NAME$$,\n\n...\n\nMit freundlichen Grüßen\n...", wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE )
		self.m_textCtrl_message.SetMaxLength( 500 ) 
		self.m_textCtrl_message.SetMinSize( wx.Size( 400,400 ) )
		
		bSizer21.Add( self.m_textCtrl_message, 0, wx.ALL|wx.EXPAND, 5 )
		
		m_sdbSizer7 = wx.StdDialogButtonSizer()
		self.m_sdbSizer7OK = wx.Button( self.m_panel10, wx.ID_OK )
		m_sdbSizer7.AddButton( self.m_sdbSizer7OK )
		self.m_sdbSizer7Cancel = wx.Button( self.m_panel10, wx.ID_CANCEL )
		m_sdbSizer7.AddButton( self.m_sdbSizer7Cancel )
		m_sdbSizer7.Realize();
		
		bSizer21.Add( m_sdbSizer7, 1, wx.EXPAND, 5 )
		
		
		self.m_panel10.SetSizer( bSizer21 )
		self.m_panel10.Layout()
		bSizer21.Fit( self.m_panel10 )
		bSizer20.Add( self.m_panel10, 1, wx.EXPAND |wx.ALL, 5 )
		
		
		self.SetSizer( bSizer20 )
		self.Layout()
		bSizer20.Fit( self )
		
		self.Centre( wx.BOTH )
	
	def __del__( self ):
		pass
	

