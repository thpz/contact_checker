#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import datetime

from peewee import fn
from model_classes import XingUser, XingGroup, XingInvitation, XingWorkingTable


class XingModel:

    def __init__(self):
        logger = logging.getLogger('peewee')
        logger.setLevel(logging.INFO)

    @staticmethod
    def add_group(group_id, group_name):
        group, _ = XingGroup.get_or_create(group_id=group_id)
        if group_name != group.name:
            print('Gruppe "{}" umbenannt in "{}"'.format(group.name, group_name))
        group.name = group_name
        group.save()

    @staticmethod
    def add_user(user_id, first_name, last_name):
        user, _ = XingUser.get_or_create(user_id=user_id)
        user.first_name = first_name
        user.last_name = last_name
        user.save()
        return user

    def add_user_to_working_list_group(self, group_id, user_id, first_name, last_name):
        group = XingGroup.get(group_id=group_id)
        user = self.add_user(user_id, first_name, last_name)
        XingWorkingTable.get_or_create(group=group, user=user)

    @staticmethod
    def invite_user_to_group(group_id, user_id):
        group = XingGroup.get(group_id=group_id)
        user = XingUser.get(user_id=user_id)

        invitation, created = XingInvitation.get_or_create(user=user, group=group)
        if not created:
            invitation.invitationDate=datetime.datetime.now()
            invitation.save()

        working_table = XingWorkingTable.get(user=user, group=group)
        working_table.is_invited = True
        working_table.save()

    @staticmethod
    def delete_working_table(group_id):
        group = XingGroup.get(group_id=group_id)
        q = XingWorkingTable.delete().where(XingWorkingTable.group == group)
        q.execute()

    @staticmethod
    def is_user_invited(user_id, group_id):
        group = XingGroup.get(group_id=group_id)
        for u in group.Invitations.select().execute():
            if u.user_id == user_id:
                return True
        return False

    @staticmethod
    def get_statistics(group_id):
        stats = dict()

        group = XingGroup.get(group_id=group_id)

        stats['xing_invited'] = XingInvitation.select().where(XingInvitation.group == group).count()
        stats['xing_workingtable'] = XingWorkingTable.select().where(XingWorkingTable.group == group).count()
        stats['xing_not_invited'] = XingWorkingTable.select().\
            where(XingWorkingTable.group == group, XingWorkingTable.is_invited == False).count()
        stats['xing_last_invitation'] = XingInvitation.select(fn.Max(XingInvitation.invitationDate)).\
            filter(XingInvitation.group == group).scalar()

        return stats

    @staticmethod
    def get_uninvited(group_id):
        group = XingGroup.get(group_id=group_id)

        users_found = list()
        q = XingUser.select().join(XingWorkingTable).\
            where(XingWorkingTable.group == group, XingWorkingTable.is_invited == False)
        for user in q.execute():
            users_found.append((user.user_id, user.last_name))

        return users_found

xing_mainmodel = XingModel()
