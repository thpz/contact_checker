#!/usr/bin/env python
# -*- coding: utf-8 -*-

from contact_checker.model_classes import db, Run, User, LinkedIn


def setup():
    db.init(':memory:')
    db.create_table(Run)
    db.create_table(User)
    db.create_table(LinkedIn)


def teardown():
    db.close()


def test_db_basic0():
    assert User.select().count() == 0
    assert LinkedIn.select().count() == 0


def test_db_basic1():
    run = Run.create()
    tom = User.create(email='tom@work.com', run=run)
    User.insert(email='ari@home.org', run=run).execute()
    LinkedIn.create(user=tom)
    assert Run.select().count() == 1
    assert User.select().count() == 2
    assert LinkedIn.select().count() == 1


def test_db_basic2():
    User.get(email='tom@work.com').delete_instance(recursive=True)
    assert User.select().count() == 1
    assert LinkedIn.select().count() == 0
