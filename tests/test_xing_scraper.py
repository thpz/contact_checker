#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from nose.tools import *

from contact_checker.xing_scraper import xing_scraper


def setup():
    pass


def test_get_all_groups():
    filename = os.path.join('.', 'tests', 'data', 'xing_html', 'get_groups.html')
    groups = xing_scraper.get_all_groups(open(filename))
    assert_equal(groups.__len__(), 39)

    for g in groups:
        if g[0] == '1067837':
            assert_equal(g[1], 'Online-PR')


def test_get_users():
    filename = os.path.join('.', 'tests', 'data', 'xing_html', 'search_users_result.html')
    users = xing_scraper.get_users(open(filename))
    assert_equal(users.__len__(), 20)

    for u in users:
        if u[0] == '3381954':
            assert_equal(u[2], u'Claudia Maria Bürger')

    link = xing_scraper.get_users_next_page_link(open(filename))
    assert_not_equal(link, None)

    filename = os.path.join('.', 'tests', 'data', 'xing_html', 'search_users_result2.html')
    link = xing_scraper.get_users_next_page_link(open(filename))
    assert_equal(link, None)


def test_get_group_members():
    filename = os.path.join('.', 'tests', 'data', 'xing_html', 'search_group_members_result.html')
    users = xing_scraper.get_group_members(open(filename))
    assert_equal(len(users), 10)

    assert_equal(users[0][0], 27775848)
    assert_equal(users[0][1], '')
    assert_equal(users[0][2], u'Mark Pochert')

    assert_equal(users[5][0], 28209986)
    assert_equal(users[5][1], '')
    assert_equal(users[5][2], u'Thomas Bauer')

    link = xing_scraper.get_group_members_next_page_link(open(filename))
    assert_not_equal(link, None)

    filename = os.path.join('.', 'tests', 'data', 'xing_html', 'search_group_members_result2.html')
    link = xing_scraper.get_group_members_next_page_link(open(filename))
    assert_not_equal(link, None)


def teardown():
    pass
