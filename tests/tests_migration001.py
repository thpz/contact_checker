#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import shutil

from peewee import IntegrityError
from nose.tools import *

from contact_checker.model import mainmodel
from contact_checker.model_classes import db, introspector, Masterversion, Run,  User, LinkedIn
from contact_checker.migrations import migrations


def setup():
    db_orig_path = os.path.join('.', 'tests', 'data', 'data_migration001.db')
    db_temp_path = os.path.join('.', 'tests', 'temp', 'temp.db')
    shutil.copyfile(db_orig_path, db_temp_path)
    db.init(db_temp_path)


def teardown():
    db.close()


def test_001():
    models = introspector.generate_models()
    contacts_model = models['CONTACTS']
    assert_equal(contacts_model.select().count(), 43)

    mainmodel.chunk_size = 5
    migrations.migration001.chunk_size = 5
    migrations.run()

    # Alle Tabellen angelegt bzw. entfernt?
    assert_false(contacts_model.table_exists())
    assert_true(Masterversion.table_exists())
    assert_true(User.table_exists())
    assert_true(LinkedIn.table_exists())

    # Masterversion
    assert_equal(Masterversion.select().count(), 1)
    assert_greater_equal(Masterversion.get().migrationLevel, 0)

    # User
    assert_equal(User.select().count(), 43)
    jupp = User.get(User.email == 'koelner1972@gmx.net')
    assert_equal(jupp.firstName, 'Josef')
    assert_equal(jupp.lastName, 'Heinemeier')
    cristian = User.get(User.email == 'cristian.achim@oce.com')
    assert_equal(cristian.firstName, 'Cristian')
    assert_equal(cristian.lastName, 'Achim')
    riccardo = User.get(User.email == 'riccardo.accetta@lenticolare.it')
    assert_equal(riccardo.firstName, 'Riccardo')
    assert_equal(riccardo.lastName, 'Accetta')
    thomas = User.get(User.email == 'thomas.acher@boschrexroth.de')
    assert_equal(thomas.firstName, 'Thomas')
    assert_equal(thomas.lastName, 'Acher')

    # Run
    assert_equal(Run.select().count(), 1)

    # Nur ein Eintrag in User pro E-Mail
    assert_raises(IntegrityError, User.create, email='koelner1972@gmx.net')

    # LinkedIn
    assert_equal(LinkedIn.select().count(), 42)
    assert_equal(LinkedIn.select().where(LinkedIn.isAlreadyConnected == True).count(), 2)
    assert_equal(LinkedIn.select().where((LinkedIn.hasLinkedIn == True) &
                                         (LinkedIn.isAlreadyConnected == False)).count(), 3)
    assert_equal(LinkedIn.select().where(LinkedIn.hasLinkedIn == False).count(), 37)

    # LinkedIn alreadyConnected
    assert_true(jupp.linkedin.get().isAlreadyConnected)
    assert_true(jupp.linkedin.get().hasLinkedIn)

    # LinkedIn withLinkedIn
    assert_false(cristian.linkedin.get().isAlreadyConnected)
    assert_true(cristian.linkedin.get().hasLinkedIn)

    # LinkedIn withoutLinkedIn
    assert_false(riccardo.linkedin.get().isAlreadyConnected)
    assert_false(riccardo.linkedin.get().hasLinkedIn)

    # LinkedIn never checked or check never finished
    assert_greater_equal(thomas.linkedin.count(), 0)

    # Nur ein Eintrag in LinkedIn pro User
    assert_raises(IntegrityError, LinkedIn.create, user=jupp)


def test_002():
    db.close()
    db.init(':memory:')
    migrations.run()

    # Alle Tabellen angelegt aber leer?
    assert_equal(Run.select().count(), 0)
    assert_equal(User.select().count(), 0)
    assert_equal(LinkedIn.select().count(), 0)

    # Ist migrations.run() eine Projektion?
    migrations.run()

    # Masterversion
    assert_equal(Masterversion.select().count(), 1)
    assert_greater_equal(Masterversion.get().migrationLevel, 0)
