#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from nose.tools import *

from contact_checker.tools.preferences import Preferences

TEMP_DIR = os.path.join('.', 'tests', 'temp')
PREFERENCES = Preferences(os.path.join(TEMP_DIR, 'preferences.json'))


def setup():
    try:
        os.remove(PREFERENCES.filename)
    except OSError:
        pass


def test_preferences():
    PREFERENCES.load_preferences()
    assert_equal(PREFERENCES.load_preferences(), {})

    assert_raises(ValueError, PREFERENCES.save_preferences('This ain\'t no dict'))
    os.remove(PREFERENCES.filename)

    PREFERENCES.set_pref('Thomas', 'Thomas Peetz')
    assert_equal(PREFERENCES.get_pref('Thomas'), 'Thomas Peetz')

    PREFERENCES.set_pref('Ariane', 'Ariane Paetz')

    PREFERENCES.set_pref('Thomas', 'Thomas Paetz')
    assert_equal(PREFERENCES.get_pref('Thomas'), 'Thomas Paetz')

    assert_equal(PREFERENCES.get_pref('Ariane'), 'Ariane Paetz')


def shutdown():
    pass
