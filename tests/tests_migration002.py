#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import shutil

from peewee import IntegrityError
from nose.tools import *

from contact_checker.model_classes import db, Masterversion, XingUser, XingGroup, XingInvitation, XingWorkingTable
from contact_checker.migrations import migrations


def setup():
    db_orig_path = os.path.join('.', 'tests', 'data', 'data_migration001.db')
    db_temp_path = os.path.join('.', 'tests', 'temp', 'temp.db')
    shutil.copyfile(db_orig_path, db_temp_path)
    db.init(db_temp_path)


def teardown():
    db.close()


def test_001():
    migrations.run()

    # Alle Tabellen angelegt bzw. entfernt?
    assert_true(XingUser.table_exists())
    assert_true(XingGroup.table_exists())
    assert_true(XingInvitation.table_exists())
    assert_true(XingWorkingTable.table_exists())

    # Masterversion
    assert_equal(Masterversion.select().count(), 1)
    assert_greater_equal(Masterversion.get().migrationLevel, 1)

    # Count entries
    assert_equal(XingUser.select().count(), 0)
    assert_equal(XingGroup.select().count(), 0)
    assert_equal(XingInvitation.select().count(), 0)
    assert_equal(XingWorkingTable.select().count(), 0)


def test_002():
    db.close()
    db.init(':memory:')
    migrations.run()

    # Alle Tabellen angelegt aber leer?
    assert_equal(XingUser.select().count(), 0)
    assert_equal(XingGroup.select().count(), 0)
    assert_equal(XingInvitation.select().count(), 0)
    assert_equal(XingWorkingTable.select().count(), 0)

    # Ist migrations.run() eine Projektion?
    migrations.run()

    # Masterversion
    assert_equal(Masterversion.select().count(), 1)
    assert_greater_equal(Masterversion.get().migrationLevel, 1)
