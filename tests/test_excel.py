#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from nose.tools import *

from contact_checker.excel_importer import ExcelImporter, NewContactsImporter


def setup():
    pass


def test_excel_001():

    class TestContactsImporter(ExcelImporter):
        @staticmethod
        def get_key():
            return ''

        @staticmethod
        def get_map():
            return {'Testspalte': 'Testspalte'}

    class TestContactsImporter2(ExcelImporter):
        @staticmethod
        def get_key():
            return ''

        @staticmethod
        def get_map():
            return {'NichtDa': '', 'AuchNichtDa': '', 'vorname': '', 'nachname': '', 'e-mail': ''}

    assert_raises(RuntimeError, TestContactsImporter.import_file, 'Datei')
    assert_raises(RuntimeError, TestContactsImporter.import_file,
                  os.path.join('.', 'tests', 'data', 'Two_Sheets.xlsx'))
    assert_raises(RuntimeError, TestContactsImporter2.import_file,
                  os.path.join('.', 'tests', 'data', '161014_TestListe.xlsx'))

    user = NewContactsImporter.parse_excel_file(os.path.join('.', 'tests', 'data', '161014_TestListe.xlsx'))
    assert_equal(user.__len__(), 3598)

    u = user[0]
    assert_equal(u['vorname'], 'Johannes')
    assert_equal(u['nachname'], 'Lindner')
    assert_equal(u['e-mail'], 'j.lindner@hofmanndruck.de')

    u = user[3597]
    assert_equal(u['vorname'], 'Jamain Arki')
    assert_equal(u['nachname'], 'Marie')
    assert_equal(u['e-mail'], 'marie.jamain@rotofrance.fr')

    assert_raises(NotImplementedError, ExcelImporter.get_map)
    assert_raises(NotImplementedError, ExcelImporter.get_key)


def teardown():
    pass
