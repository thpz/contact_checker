#!/usr/bin/env python
# -*- coding: utf-8 -*-
from nose.tools import *

from contact_checker.model_classes import db
from contact_checker.migrations import migrations
from contact_checker.xing_model import xing_mainmodel
from contact_checker.model_classes import XingUser, XingGroup, XingInvitation, XingWorkingTable


def setup():
    db.init(':memory:')
    migrations.run()


def test_xing_model_001():
    for i in range(10):
        xing_mainmodel.add_group(i, 'Gruppe_{}'.format(i))

    assert_raises(xing_mainmodel.add_group(5, 'Gruppe_5'))
    assert_equal(XingGroup.select().count(), 10)

    group_4 = XingGroup.get(group_id=4)
    assert_equals(group_4.name, 'Gruppe_4')


def test_xing_model_002():
    xing_mainmodel.add_user(1, '00', '7')
    user_1 = XingUser.get(user_id=1)
    assert_equals(user_1.first_name, '00')
    assert_equals(user_1.last_name, '7')

    xing_mainmodel.add_group(666, 'Hell')
    xing_mainmodel.add_user_to_working_list_group(666, 1, '1', '11')
    user_1 = XingUser.get(user_id=1)
    assert_equals(user_1.first_name, '1')
    assert_equals(user_1.last_name, '11')

    for i in range(10):
        for j in range(i):
            xing_mainmodel.add_user_to_working_list_group(i, j, 'First Name {}'.format(j), 'Last Name {}'.format(j))

    for i in range(10):
        group = XingGroup.get(group_id=i)
        assert_equal(group.ListedUsers.select().count(), i)

    for j in range(0, 9, 2):
        xing_mainmodel.add_user_to_working_list_group(9, j, 'Vorname {}'.format(j), 'Nachname {}'.format(j))

    for i in range(10):
        group = XingGroup.get(group_id=i)
        assert_equal(group.ListedUsers.select().count(), i)

    user_1 = XingUser.get(user_id=1)
    assert_equals(user_1.first_name, 'First Name 1')
    assert_equals(user_1.last_name, 'Last Name 1')
    assert_equals(user_1.isListedFor.select().count(), 9)

    user_6 = XingUser.get(user_id=6)
    assert_equals(user_6.first_name, 'Vorname 6')
    assert_equals(user_6.last_name, 'Nachname 6')
    assert_equals(user_6.isListedFor.select().count(), 3)


def test_xing_model_003():
    for i in range(1, 10):
        xing_mainmodel.invite_user_to_group(i, i - 1)

    for i in range(9):
        user = XingUser.get(user_id=i)
        assert_equal(user.isInvitedTo.select().count(), 1)

    for i in range(1, 10):
        group = XingGroup.get(group_id=i)
        assert_equal(group.Invitations.select().count(), 1)

    assert_equal(XingInvitation.select().count(), 9)


def test_xing_model_004():
    assert_equal(XingWorkingTable.select().count(), 46)

    xing_mainmodel.delete_working_table(6)
    assert_equal(XingWorkingTable.select().count(), 40)

    xing_mainmodel.delete_working_table(4)
    assert_equal(XingWorkingTable.select().count(), 36)


def teardown():
    db.close()
