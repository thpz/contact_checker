#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

from nose.tools import *

from contact_checker.model import db, mainmodel, Run, User
from contact_checker.migrations import migrations
from contact_checker.controller import Controller


def setup():
    mainmodel.chunk_size = 500


def test_controller_001():
    Controller.connect2database(':memory:')


def test_controller_002():
    assert_raises(RuntimeError, Controller.load_new_contacts_from_excel, filename='xxx')
    assert_raises(RuntimeError, Controller.load_known_contacts_from_excel,
                  filename=os.path.join('.', 'tests', 'data', 'Two_Sheets.xlsx'))

    filename = os.path.join('.', 'tests', 'data', '161014_TestListe.xlsx')
    Controller.load_new_contacts_from_excel(filename=filename)

    assert_equals(User.select().count(), 3252)

    user = User.get(User.email == 'j.lindner@hofmanndruck.de')
    assert_equal(user.firstName, 'Johannes')
    assert_equal(user.lastName, 'Lindner')

    user = User.get(User.email == 'zeev@stickler.co.il')
    assert_equal(user.firstName, 'Zeev')
    assert_equal(user.lastName, 'Mankuta')


def test_controller_003():
    filename = os.path.join('.', 'tests', 'data', 'linkedin_connections_export_outlook_express.xlsx')
    Controller.load_known_contacts_from_excel(filename=filename)

    assert_equals(User.select().count(), 3252 + 196)

    user = User.get(User.email == 'frank.westermeyer@hesge.ch')
    assert_equal(user.firstName, 'Frank')
    assert_equal(user.lastName, 'Westermeyer')

    double_user = {
        'run': user.run,
        'email': user.email,
        'lastName': user.lastName,
        'firstName': user.firstName
    }
    assert_raises(RuntimeError, mainmodel.insert_many, User, [double_user])


def test_controller_004():
    stat = Controller.get_statistics()
    assert_equals(stat.contacts, 3252 + 196)
    assert_equals(stat.in_linkedin_group, 196)
    assert_equals(stat.not_in_linkedin_group, 3252)
    assert_equals(stat.linkedin_unchecked, 3252)
    assert_equals(stat.with_linkedin_account, 0)
    assert_equals(stat.without_linkedin_account, 0)
    assert_equals(stat.not_invited_to_linkedin_group, 0)
    assert_equals(stat.invited_to_linkedin_group, 0)


def teardown():
    db.close()
