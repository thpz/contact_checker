#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pip
import sys
import tempfile

# Packages to ignore
IGNORE = ['wyPython', ]

# Redirect stdout to tempfile
sys.stdout = tempfile.NamedTemporaryFile(delete=False)
pip.main(['freeze'])
sys.stdout.close()

# Copy tempfile to requirements.txt, skip wxPython etc.
with open(sys.stdout.name, 'r') as tmp, open('./requirements.txt', 'w') as target:
    [target.write(l) for l in tmp if not l.startswith(tuple(IGNORE))]
